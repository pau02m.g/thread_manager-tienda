package Tienda;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;



public class Main {
	public static void main(String[] args) {
	
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Gestion g = new Gestion();
		g.capacidad.add(new Object());
		
		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();
		
		futuros.add(executor.submit(new Clientes(g)));
		futuros.add(executor.submit(new Clientes(g)));
		futuros.add(executor.submit(new Clientes(g)));
		futuros.add(executor.submit(new Clientes(g)));
		futuros.add(executor.submit(new Clientes(g)));
		
		
		
//		try {
//			while(true) {
//				Thread.sleep(3000);
//				executor.submit(new Clientes(g));
//			}
//		}
//		catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//		}
		
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		// cuando todos acaban
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		
	}
}
