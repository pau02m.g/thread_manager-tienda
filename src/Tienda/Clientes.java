package Tienda;

import java.util.concurrent.Callable;

public class Clientes implements Callable<Boolean>{


	
	private Gestion m_gestion;
	
	public Clientes(Gestion g) {
		m_gestion = g;
	}
	
	
	private boolean atendido = false;
	private boolean cobrado = false;
	@Override
	public Boolean call() {
		
		try {
			while(!atendido) {
				if(!m_gestion.Pasar()) {
					System.out.println(Thread.currentThread().getName() + "			|		Ni�o me toca ya");
					synchronized (m_gestion.siguiente) {
						m_gestion.siguiente.wait();
					}	
				}
				else {
					while(!cobrado) {
						if(!m_gestion.Cobrar()) {
							synchronized (m_gestion.caja) {
								m_gestion.caja.wait();
							}
						}
						else {
							Thread.sleep(1000);
							m_gestion.Siguiente_cliente();
							
							synchronized (m_gestion.caja) {
								m_gestion.caja.notifyAll();
							}
							synchronized (m_gestion.siguiente) {
								m_gestion.siguiente.notifyAll();
							}
							
							System.out.println(Thread.currentThread().getName() +"			|		- que rapido tapa el pollo mama\n 						- eso es que tiene mucha practica");

							cobrado = true;
							atendido = true;
							
						}
						
					}
				}
			}
		}
		catch (InterruptedException e) {
			System.out.println("peto");
		}

		
		
		return true;
	}

}

